from battleship.grid import PlayerGrid, OpponentGrid;
from battleship.helpers import Error;
from battleship.menu import Menu;
import re;

class Game():

    def __init__(
            self,
            allowed_ships = { 1: 1},
            grid = PlayerGrid,
            opponent_grid = OpponentGrid,
            conn = None,
            server = None
    ):
        self.allowed_ships = allowed_ships;
        self.grid = grid();
        self.opponent_grid = opponent_grid();
        self.ships = {};
        self.conn = conn;
        self._server = server;
        self._next_move = 1 if self._server else 0;
        self.debug = 1;

    def add_ships(self):
        print("Allowed ships: {}".format( self.allowed_ships ));

        menu_items = {
            '1': ('Add ship', self.add_ship),
            '2': ('Delete ship', self.delete_ship),
            '3': ('Move ship', self.move_ship),
            's': ('Show grid', lambda: print(self.grid) ),
            'q': ('Done', lambda: print("\nMust add all ships to continue!\n") if sum( self.allowed_ships.values() ) != 0 else "__ENDMENU__"),
        };

        m = Menu( menu_items );
        m.enter();

    def add_ship(self):

        while 1:
            size = input("Select ship to add (enter size): ").strip();
            try:
                size = int(size);
                break;
            except:
                pass;
            
        if not self.allowed_ships.get(size):
            print("No ship available: {}".format( size ));
            return;

        while 1:
            coords = self._input_coordinates();
            if not coords:
                continue;
            if (size == 1 and len(coords) == 1) or \
               (size > 1 and len(coords) == 2 and ( (size - 1 ) ** 2 == ( coords[0][0] - coords[1][0] ) ** 2 + ( coords[0][1] - coords[1][1] ) ** 2 ) ):
                break;
            else:
                print( "Wrong ship coordinates" );

        if size == 1:
            coords.append( coords[0] );
            
        try:
            self.grid.add_ship( *coords );
            self.allowed_ships[size] -= 1;
        except Exception as err:
            print( f"\n{ err }\n" );
        
    def delete_ship(self):
        coord = input( "Enter ship coordinate (any, e.g. '1-1'): ").strip();
        coord = tuple( int(x) for x in coord.split("-") );
        ship_id = self.grid.get_ship_id_by_coord( coord );
        if ship_id:
            self.grid.delete_ship( ship_id );
        else:
            print("No ship selected");

    def move_ship(self):
        coord = input( "Enter ship coordinate (any, e.g. '1-1'): ").strip();
        coord = tuple( int(x) for x in coord.split("-") );
        ship_id = self.grid.get_ship_id_by_coord( coord );
        if ship_id:
            coord = self._input_coordinates();
            self.grid.move_ship( ship_id, *coord );
        else:
            print("No ship selected");

    def _input_coordinates(self, msg = "Enter coordinates (e.g., '1-1 1-3')"):
        """Prompt user for ship coordinates and return result."""

        result = [];

        while 1:
            val = input(f'{ msg }: ').strip();
            val = val.split();
            for el in val:
                coord = el.split("-");
                if not ( len(coord) == 2 and coord[0].isdigit() and coord[1].isdigit() ):
                    print( "Incorrect input, try again" );
                    break;
                else:
                    result.append( tuple(int(x) for x in coord) );
            else:
                break;
            
        return result;


    def play(self):

        self.conn.send( "READY\n".encode() );
        while 1:
            if self.conn.receive().decode() == "READY":
                break;

        while self._next_move is not None:
            print(self.grid);
            print(self.opponent_grid);

            if self._next_move:
                self.make_move();

            else:
                self.wait_move();
                    
                
    def shoot(self):

        """Sends SHOOT message and receives reply."""
        
        while 1:
            coord = self._input_coordinates(msg = "Enter one coordinate, e.g. '1-1'");
            if len(coord) == 1:
                break;
            
        (row, col) = coord[0];

        msg = f"SHOOT { row } { col }\n";
        self.conn.send( msg.encode() );

        reply = self.conn.receive().decode();
        print( f"=> Received '{ reply }'" );
        cmd = self._parse_msg( reply );

        if cmd[0] == "MESSAGE ERROR":
            print("Received error, please try again");
            return;

        if cmd[0] in [ "HIT", "MISS", "SUNK", "ENDGAME" ]:
            self.opponent_grid.mark(row, col,
                                    1 if cmd[0] == "HIT" or cmd[0] == "SUNK" else 0,
                                    1 if cmd[0] == "SUNK" else 0);
        if cmd[0] == "MISS":
            self._next_move = 0;

            

        if cmd[0] == "ENDGAME":
            print( "\n\nYou won!\n\n" );
            self._next_move = None;

    def send_msg(self, msg):
        self.conn.send( msg.encode() );

            
    def menu_move_ship(self):

        menu = {
            
        };

    def make_move(self):

        print("=> Your move");

        self.shoot();
        

    def wait_move(self):
        """Receive opponent's move"""
        
        print("=> Opponent's move");

        while 1:
            msg = self.conn.receive().decode();
            print( f"=> Received '{ msg }'" );
            cmd = self._parse_msg( msg );
            if not cmd:
                self.conn.send( "MESSAGE ERROR\n".encode() );
                continue;

            if cmd[0] == "SHOOT":
                try:
                    result = self.grid.shoot( cmd[1], cmd[2] );
                    
                    if result == "MISS":
                        self._next_move = 1;

                    self.conn.send( f"{result}\n".encode() );

                    if result == "ENDGAME":
                        print( "\nGame over\n" );
                        self._next_move = None;
                    
                    break;
                except:
                    self.conn.send( "MESSAGE ERROR\n".encode() );

        

    def _parse_msg(self, msg):

        match = re.match( r"(SHOOT) (\d+) (\d+)$", msg );
        if match:
            return ( "SHOOT", int(match.group(2)), int(match.group(3)) );
        if msg in [ "MESSAGE ERROR", "HIT", "MISS", "SUNK", "ENDGAME" ]:
            return ( msg, );

        return None;
    
if __name__ == "__main__":
    game = Game();
    #game.add_ships();
    print( game._input_coordinates() );


