from battleship import ship, helpers;
from battleship.helpers import vector_points;
from battleship.ship import Ship;

class Square():
    def __init__(self, row, col):
        self._row = row;
        self._col = col;
        

class Grid():
    def __init__(self, size=10, square=Square):
        self._size = size;
        self._square_class = square;
        self._squares = {};

class PlayerSquare(Square):

    _shots = 0;
    _ship_id = None;
    
    def __init__(self, row, col, ship_id=None):
        super().__init__(row, col);
        if ship_id:
            self._ship_id = ship_id;

    def shoot(self):
        self._shots += 1;

    def get_ship_id(self):
        return self._ship_id;

        
class PlayerGrid(Grid):
    """Class for the player map."""

    def __init__(self: object, size: int = 10, square=PlayerSquare):
        if not (size > 0):
            raise helpers.Error("Map size must be >= 1");
        
        super().__init__(size=size, square=square)
        self._ships = {};
        self._squares = {};
        self._ship_id = 0;


    def __str__(self):


        ship_squares = self.get_ship_squares();
        
        s = [];
        s.append( ' '.join( ['  '] + [ str(x) for x in range(1, self._size + 1) ] ) );
        
        for row in range(1, self._size + 1):
            l = [ "{:2}".format(row) ];
            for col in range(1, self._size + 1):
                ship_id = ship_squares.get( (row, col) );
                if ship_id:
                    if self._ships[ ship_id ].is_hit( row, col ):
                       l.append("X");
                    else:
                        l.append("O");
                else:
                    l.append(".");
            s.append( ' '.join( l ) );
        return '\n'.join(s);

    def get_ship_squares(self):
        squares = {};
        for ship_id, ship in self._ships.items():
            for coord in ship.get_all_coordinates():
                squares[ coord ] = ship_id;

        return squares;

    def get_ship_id_by_coord(self, coord):
        return self.get_ship_squares().get( coord );

    def move_ship(self, ship_id, begin, end):
        self._check_ship_coordinates(begin, end, ship_id);
        self._ships[ ship_id ].set_position( begin, end );
    
    def shoot(self, row, col):
        """Takes coordinates to shoot at and returns result."""

        id = (row, col);
        if not self._check_coordinate( row, col ):
            raise helpers.Error("Wrong coordinate");

        ship_squares = self.get_ship_squares();
        
        ship_id = ship_squares.get( id );
        if ship_id:
            result = self._ships[ ship_id ].shoot( row, col );
        else:
            result =  "MISS";

        if result == "SUNK" and not self.ships_afloat():
            result = "ENDGAME";

        return result;
            
    def ships_afloat(self):

        for ship in self._ships.values():
            print( f"DEBUG => ship { ship } is afloat: { ship._afloat }" );
            if ship._afloat:
                return True;

        return False;
        
    def add_ship(self, coord_start, coord_end):
        """Adds ship to the map."""

        ship_id = self._gen_ship_id();

        self._check_ship_coordinates(coord_start, coord_end);
        
        self._ships[ ship_id ] = Ship(coord_start, coord_end);

    def delete_ship(self, ship_id):

        if ship_id in self._ships:
            del self._ships[ ship_id ];

    def _check_coordinate(self, row, col):
        """Check one coordinate."""
        return (
            row >= 1 and row <= self._size
        ) and (
            col >= 1 and col <= self._size
        );

    def _check_ship_coordinates(self, coord_start, coord_end, ship_id = None):
        
        for coord in (coord_start, coord_end):
            if not (coord[0] >= 1 and coord[0] <= self._size and
                    coord[1] >= 1 and coord[1] <= self._size):
                raise helpers.Error("Coordinates are out of range!");

        ship_squares = self.get_ship_squares();
        for coord in vector_points(coord_start, coord_end):
            
            if coord in ship_squares:
                if not ( ship_id and (ship_id == ship_squares[ coord ]) ):
                    raise helpers.Error("Can't have overlapping ships!");

            (row, col) = coord;
            for r, c in (
                    (row -1 , col),
                    (row - 1, col +1),
                    (row, col + 1),
                    (row + 1, col + 1),
                    (row + 1, col),
                    (row + 1, col - 1),
                    (row, col - 1),
                    (row - 1, col - 1)
            ):
                square_ship_id = ship_squares.get( (r, c) );
                if square_ship_id:
                    if ship_id and (ship_id == square_ship_id):
                        continue;
                    raise helpers.Error("Can't have adjacent ships!");

    
    def _gen_ship_id(self):
        self._ship_id += 1;
        return self._ship_id;
        
class OpponentSquare():

    def __init__(self):
        self._shot_count = 0;
        self._ship_id = None;
        self._sunk = None;
    
class OpponentGrid(Grid):

    def __init__(self):
        super().__init__();
        self._ships = {};
        self._ship_id = 0;

    def mark(self, row, col, hit=False, sunk=False):


        square = self._squares.get( (row, col) );
        if not square:
            square = OpponentSquare();
            self._squares[ (row, col) ] = square;

        square._shot_count += 1;
        
        if hit:
            if square._ship_id is None:
                # search adjacent squares for ship_id
                for r, c in (
                        (row - 1, col),
                        (row, col + 1),
                        (row + 1, col),
                        (row, col - 1) ):
                    s = self._squares.get( (row, col) );
                    if s:
                        if s._ship_id:
                            square._ship_id = s._ship_id;
                            break;
                        
                if square._ship_id is None:
                    square._ship_id = self._ship_id;
                    self._ship_id += 1;

        if sunk:
            for s in self._squares.values():
                if s._ship_id == square._ship_id:
                    s._sunk = 1;

    def __str__(self):
        
        s = [];
        s.append( ' '.join( ['  '] + [ str(x) for x in range(1, self._size + 1) ] ) );
        
        for row in range(1, self._size + 1):
            l = [ "{:2}".format(row) ];
            for col in range(1, self._size + 1):
                square = self._squares.get( (row, col) );
                if square:
                    if square._ship_id is not None:
                        if square._sunk:
                            l.append("x");
                        else:
                            l.append("o");
                    else:
                        l.append("*");
                else:
                    l.append(".");
            s.append( ' '.join( l ) );
        return '\n'.join(s);

                    
        
if __name__ == "__main__":

    # g = PlayerGrid();
    # print(g);
    # g.add_ship( (2,5), (2,10)  );
    # g.add_ship( (4,4), (5,4)  );
    # g.add_ship( (10,4), (10,3)  );
    # print(g);


    g = OpponentGrid();
    g.mark(1, 1, 1, 0);
    g.mark(1, 1, 1, 1);
    print(g);
