from battleship.helpers import vector_points
import random;

class Ship():

    def __init__(self, begin, end, afloat=1):
        """Takes tuples of coordinates of begin and end."""
        self._begin = begin;
        self._end = end;
        self._afloat = afloat;
        self._compartments = {};

        for coord in vector_points(self._begin, self._end):
            c = Compartment();
            self._compartments[ coord ] = c;


    def get_position(self):
        return ( self._begin, self._end );

    def set_position(self, begin, end):

        updated_compartments = {};
        for old_coord, new_coord in zip( self._compartments, vector_points(begin, end) ):
            updated_compartments[ new_coord ] = self._compartments[ old_coord ];

        self._compartments = updated_compartments;

    def is_hit(self, row, col):
        return self._compartments[ (row, col) ].is_hit();

    def get_all_coordinates(self):
        return self._compartments.keys();
    
    def shoot(self, row, col):

        
        c = self._compartments.get( (row, col) );
        if c:
            c.hit();
            self._afloat = self._check_afloat();
            if self._afloat:
                return "HIT";
            else:
                return "SUNK";
        
        return "MISS";

    def is_afloat(self):
        return self._afloat;

    def _check_afloat(self):
        for c in self._compartments.values():
            if not c.is_hit():
                return 1;
        return 0;


class Compartment():

    def __init__(self, hit=0):
        self._hit = hit;

    def hit(self):
        self._hit = 1;

    def is_hit(self):
        return self._hit;


# ship = Ship(  ('a', 10), ('a', 10) );

if __name__ == "__main__":
    ship = Ship( (1, 1), (1, 1) );
    #print( ship.get_position() );
    print( ship._compartments );
    # for c in ship._compartments.values():
    #     c.hit();
    #     print('Ship afloat: {}'.format( ship._check_afloat() ));
        
