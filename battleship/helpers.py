class Error(Exception):
    pass;


def vector_points(coord_start, coord_end):

    points = [];

    (start, end) = sorted( (coord_start, coord_end) );

    if start[0] == end[0]:
        row = start[0];
        for col in range(start[1], end[1] + 1):
            points.append( (row, col) );
    elif start[1] == end[1]:
        col = start[1];
        for row in range(start[0], end[0] + 1):
            points.append( (row, col) );
    else:
        raise helpers.Error("Wrong coordinates");

    return points if (start, end) == (coord_start, coord_end) else points[::-1];


if __name__ == "__main__":
    print( vector_points( (1,1), (1,1) ) );
