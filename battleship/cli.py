"""
battleship.cli

Game CLI
"""

import sys;

import battleship.grid;
from battleship.game import Game;
from battleship.menu import Menu;
from battleship.network import Server, Client, Connection;
#from battleship.network import make_server, make_client;

def show_menu():
    """Print menu and process user's choice."""
    for cmd, action in menu.items():
        print(f'{ cmd }. { action[0] }');

        
def run_cmd():
    """Read user command and execute it."""
    cmd = None;
    try:
        cmd = input("\nEnter command: ").strip();
    except EOFError:
        print("");
        sys.exit();
    if cmd:
        action = menu.get(cmd);
        if action:
            action[1]();
        else:
            cmd_error(cmd);
            

        
def cmd_error(cmd):
    """Print error if """
    print(f"{ cmd }: Unknown command");


def action_new_game():

    menu = {
        "1": ("Create game", create_new_game),
        "2": ("Join game", join_game),
        "3": ("TODO: play with computer", lambda: None),
        "4": ("Exit", lambda: "__ENDMENU__"),
    };

    Menu( menu ).enter();


def create_new_game():
    print("Waiting for connection");
    conn = Server( address = '', port = 10000 ).wait_connection();

    game = Game(
        allowed_ships=allowed_ships,
        conn = conn,
        server = 1,
    );
    game.add_ships();
    game.play();

def join_game():
    addr = input("Enter server address: ").strip();
    conn = Client( addr, 10000 ).connect();

    game = Game(
        allowed_ships=allowed_ships,
        conn = conn,
    );
    game.add_ships();
    game.play();
    
    
    
def action_test_game():
    game = Game(
        allowed_ships=allowed_ships,
    );

    menu = {
        "1": ("Manipulate ships", game.add_ships),
        "2": ("Shoot", game.shoot),
        "q": ("End", lambda: "__ENDMENU__")
    };

    Menu( menu ).enter();
    
    
menu = {
    '1': ('New game', action_new_game),
    '2': ('Test game', action_test_game),
    'q': ('Exit', sys.exit),
};

allowed_ships = { 4: 1, 2: 1, 1: 1};
#allowed_ships = { 1: 1 };

def main():
    show_menu();
    while 1:
        run_cmd();

if __name__ == "__main__":
    main();
