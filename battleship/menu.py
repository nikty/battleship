class Menu():

    def __init__(self, menu, end_callback=None):
        self.menu = menu;

    def show(self):
        for cmd, action in self.menu.items():
            print( f'{ cmd }. { action[0] }' );

    def run_cmd(self):
        cmd = None;
        cmd = input( "\nEnter command: ").strip();

        if cmd:
            action = self.menu.get( cmd );
            if action:
                return action[1]();
            else:
                print( f'Unknown command: { cmd }' );

    def enter(self):
        while 1:
            self.show();
            if self.run_cmd() == "__ENDMENU__":
                return;

