import socket;

class Server:

    def __init__(self, address, port):
        self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM );
        self.sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1);
        self.sock.bind( (address, port) );
        self.sock.listen();
        
    def wait_connection(self):
        ( client_socket, address ) = self.sock.accept();
        print( f"Connection from { address }" );
        return Connection( client_socket );



class Client:
    def __init__(self, address, port):
        self.address = address;
        self.port = port;
        self.conn = Connection();

    def connect(self):
        self.conn.connect( self.address, self.port );
        return self.conn;

class Connection:

    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM );
        else:
            self.sock = sock;
            
        self._recv_buffer = b'';

    def connect(self, host, port):
        self.sock.connect( (host, port) );

    def send(self, msg):
        totalsent = 0;
        msg_len = len(msg);
        while totalsent < msg_len:
            sent = self.sock.send( msg[totalsent:] );
            if sent == 0:
                raise RuntimeError("socket connection broken");
            totalsent += sent;

    def receive(self):

        bytes_received = 0;
        while 1:
            chunk = self.sock.recv(1024);
            if chunk == b'':
                raise RuntimeError("socket connection broken");

            delimeter_pos = chunk.find(b"\n");
            if delimeter_pos != -1:
                msg = self._recv_buffer + chunk[:delimeter_pos];
                self._recv_buffer = chunk[delimeter_pos+1:];
                return msg;
            
            self._recv_buffer += chunk;
            bytes_received += len(chunk);        


if __name__ == "__main__":
    pass;
